import 'dart:async';
import 'package:bloc_basics/events/events1.dart';

class CounterBloc {
  int _cntr = 0;

  // state controler - stream controller:
  final _cntrStateController = StreamController<int>();

  // input
  StreamSink<int> get _inCntr => _cntrStateController.sink;

  // output
  // for example stream in StremaBuilder in Widget
  Stream<int> get cntr => _cntrStateController.stream;

// For events:
  final _cntrEventController = StreamController<CounterEvent>();

  // only expose a sink (input)
  Sink<CounterEvent> get cntrEventSink => _cntrEventController.sink;

  // have to connect stream with sink, therefor have to listen to the _cntrEventController:
  CounterBloc() {
    _cntrEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(CounterEvent ev) {
    if (ev == CounterEvent.increment) {
      _cntr++;
    } else {
      _cntr--;
    }
    // adding new cntr value to the sink:
    _inCntr.add(_cntr);
  }

  // close stream controllers:
  void dispose() {
    _cntrStateController.close();
    _cntrEventController.close();
  }
}
